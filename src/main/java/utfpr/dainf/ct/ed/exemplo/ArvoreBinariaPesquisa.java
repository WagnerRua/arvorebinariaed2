/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.dainf.ct.ed.exemplo;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Exemplo de implementação de árvore binária de pesquisa.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 * @param <E> O tipo do valor armazenado nos nós na árvore
 */
public class ArvoreBinariaPesquisa<E> extends ArvoreBinaria<E> {
    private ArvoreBinariaPesquisa<E> pai;
    private ArvoreBinariaPesquisa<E> esquerda;
    private ArvoreBinariaPesquisa<E> direita;

    public ArvoreBinariaPesquisa() {
    }

    public ArvoreBinariaPesquisa(E dado) {
        super(dado);
    }

    protected ArvoreBinariaPesquisa<E> getPai() {
        return pai;
    }

    @Override
    protected ArvoreBinariaPesquisa<E> getEsquerda() {
        return esquerda;
    }
    
    @Override
    protected ArvoreBinariaPesquisa<E> getDireita() {
        return direita;
    }
    
    @Override
    public ArvoreBinariaPesquisa<E> insereEsquerda(E dado) {
        ArvoreBinariaPesquisa<E> e = esquerda;
        esquerda = new ArvoreBinariaPesquisa<>(dado);
        esquerda.esquerda = e;
        return esquerda;
    }
    
    @Override
    public ArvoreBinariaPesquisa<E> insereDireita(E dado) {
        ArvoreBinariaPesquisa<E> d = direita;
        direita = new ArvoreBinariaPesquisa<>(dado);
        direita.direita = d;
        return direita;
    }
    
    public void visitaEmOrdem(ArvoreBinariaPesquisa<E> raiz) {
        if (raiz != null) {
            this.visitaEmOrdem(raiz.esquerda);
            visita(raiz);
            this.visitaEmOrdem(raiz.direita);
        }
    }
    
    @Override
    public void visitaEmOrdem() {
        visitaEmOrdem(this);
    }
    
    protected void setPai(ArvoreBinariaPesquisa<E> pai) {
        this.pai = pai;
    }
    
    public ArvoreBinariaPesquisa<E> pesquisa(E dado) {
        ArvoreBinariaPesquisa<E> aux  = this;
        
        while(aux != null && dado != aux.getDado())
        {
            if(((Comparable)dado).compareTo((Comparable)aux.getDado())<0)
                aux = aux.getEsquerda();
            else
                aux = aux.getDireita();
        }
        return (aux);
    }

    public ArvoreBinariaPesquisa<E> getMinimo() {
        ArvoreBinariaPesquisa<E> no = this;
        
        while(no != null && no.getEsquerda() != null){
            no = no.getEsquerda();
        }
        return(no);
    }

    public ArvoreBinariaPesquisa<E> getMaximo() {
        ArvoreBinariaPesquisa<E> no = this;
        
        while(no != null && no.getDireita() != null){
            no = no.getDireita();
        }
        return(no);
    }

    public ArvoreBinariaPesquisa<E> sucessor(ArvoreBinariaPesquisa<E> no) {
        if(no != null && no.getDireita() != null){
            return (no.getDireita().getMinimo());
        }
        ArvoreBinariaPesquisa<E> pai = no.pai;
        while(pai != null && no == pai.getDireita()){
            no = pai;
            pai = pai.pai;
        }
        return(pai);
    }

    public ArvoreBinariaPesquisa<E> predecessor(ArvoreBinariaPesquisa<E> no) {
        if(no != null && no.getEsquerda() != null){
            return (no.getEsquerda().getMaximo());
        }
        ArvoreBinariaPesquisa<E> pai = no.pai;
        while(pai != null && no == pai.getEsquerda()){
            no = pai;
            pai = pai.pai;
        }
        return(pai);
    }

    public ArvoreBinariaPesquisa<E> insere(E dado) {
        ArvoreBinariaPesquisa<E> aux = null;
        ArvoreBinariaPesquisa<E> raiz = this;        
        ArvoreBinariaPesquisa<E> futuro_pai = null;
        
        while(raiz != null){
            futuro_pai = raiz;
            if(((Comparable)dado).compareTo((Comparable)raiz.getDado())<0){
                raiz = raiz.getEsquerda();
            }
            else{
                raiz = raiz.getDireita();
            }
        }
        if(futuro_pai == null){
            raiz.setDado(dado);
            return raiz;
        }
        else{
            if(((Comparable)dado).compareTo((Comparable)futuro_pai.getDado())<0){
                aux = futuro_pai.insereEsquerda(dado);
                aux.setPai(futuro_pai);
                return aux;
            }
            else{
                aux = futuro_pai.insereDireita(dado);
                aux.setPai(futuro_pai);
                return aux;
            }
        }
    }

    public ArvoreBinariaPesquisa<E> exclui(ArvoreBinariaPesquisa<E> no) {
        if(no == null)
            return null;
        else if(no.getDireita() == null && no.getEsquerda() == null)
        {
            if(no.pai.getDireita() == no)
                no.pai.direita = null;
            if(no.pai.getEsquerda() == no)
                no.pai.esquerda = null;
        }
        else if(no.getDireita() == null && no.getEsquerda() != null)
        {
            if(no.pai.getDireita() == no)
                no.pai.direita = no.getEsquerda();
            if(no.pai.getEsquerda() == no)
                no.pai.esquerda = no.getEsquerda();
        }
        else if(no.getDireita() != null && no.getEsquerda() == null)
        {
            if(no.pai.getDireita() == no)
                no.pai.direita = no.getDireita();
            if(no.pai.getEsquerda() == no)
                no.pai.esquerda = no.getDireita();
        }
        else
        {
            ArvoreBinariaPesquisa<E> aux = no.sucessor(no);
            while(aux.getEsquerda() != null)
                aux = sucessor(aux);
            
            
            if(aux.getDireita() != null)
            {
                aux.esquerda = no.esquerda;
                no = aux;
                
            }
            else
            {
                no.setDado(aux.getDado());
                aux.pai.esquerda = null;
            }
            
        }
        
        return no;
    }
}
